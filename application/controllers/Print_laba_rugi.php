<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Print_laba_rugi extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
    {
        parent::__construct();
        $this->load->model('M_transaksi');
        $this->load->model('M_outlet');
    }

	public function index(){
        $bulan=$this->input->get('bulan');
        $tahun=$this->input->get('tahun');
		$expense=0;
        $income=0;
        $totalbiaya=0;
        $totalbiayatetap=0;
        $totalbiayatidaktetap=0;
        $laporan = $this->M_transaksi->laporan("WHERE MONTH(tgl_transaksi)='$bulan' and YEAR(tgl_transaksi)='$tahun'", $this->input->get('kd_outlet'))->result();
        $bebanbiayatetap = $this->M_transaksi->bebanbiaya("WHERE jenis_biaya='0'", $this->input->get('kd_outlet'))->result();
        $jml_data_biayatetap= $this->M_transaksi->bebanbiaya("WHERE jenis_biaya='0'", $this->input->get('kd_outlet'))->num_rows();
        $bebanbiaya = $this->M_transaksi->bebanbiaya("WHERE MONTH(tgl_biaya)='$bulan' and YEAR(tgl_biaya)='$tahun' and jenis_biaya='1'", $this->input->get('kd_outlet'))->result();
        $jml_data_biaya= $this->M_transaksi->bebanbiaya("WHERE MONTH(tgl_biaya)='$bulan' and YEAR(tgl_biaya)='$tahun' and jenis_biaya='1'", $this->input->get('kd_outlet'))->num_rows();

        foreach ($laporan as $data_laporan) {
            if ($data_laporan->jenis_transaksi=="0") {
                $income=$income+$data_laporan->harga_total;
            }
            if ($data_laporan->jenis_transaksi=="1") {
                $expense=$expense+$data_laporan->harga_total;
            }
        }

        foreach ($bebanbiayatetap as $data_bebanbiayatetap) {
            if ($data_bebanbiayatetap->jenis_biaya_per==0) {
                $data_bebanbiayatetap->jumlah_biaya=$data_bebanbiayatetap->jumlah_biaya;
            }else{
                $data_bebanbiayatetap->jumlah_biaya=$data_bebanbiayatetap->jumlah_biaya/12;
            }
            $totalbiayatetap=$totalbiayatetap+$data_bebanbiayatetap->jumlah_biaya;
        
        }

        foreach ($bebanbiaya as $data_bebanbiaya) {
            $totalbiayatidaktetap=$totalbiayatidaktetap+$data_bebanbiaya->jumlah_biaya;
        }


        $totalbiaya=$totalbiayatetap+$totalbiayatidaktetap;

        if ($bulan==1) {
        	$bulan="Januari";
        }elseif ($bulan==2) {
        	$bulan="Februari";
        }elseif ($bulan==3) {
        	$bulan="Maret";
        }elseif ($bulan==4) {
        	$bulan="April";
        }elseif ($bulan==5) {
        	$bulan="Mei";
        }elseif ($bulan==6) {
        	$bulan="Juni";
        }elseif ($bulan==7) {
        	$bulan="Juli";
        }elseif ($bulan==8) {
        	$bulan="Agustus";
        }elseif ($bulan==9) {
        	$bulan="September";
        }elseif ($bulan==10) {
        	$bulan="Oktober";
        }elseif ($bulan==11) {
        	$bulan="November";
        }elseif ($bulan==12) {
        	$bulan="Desember";
        }

        $outlet=$this->M_outlet->get_by_kd($this->input->get('kd_outlet'));

        $data = array(
            'nama_outlet'           => $outlet->nama_outlet,
            'alamat_outlet'         => $outlet->alamat_outlet,
            'deskripsi_outlet'      => $outlet->deskripsi,
            'no_telp_outlet'        => $outlet->no_telp,
            'nama_outlet'           => $outlet->nama_outlet,
        	'periode'		        => $bulan." ".$tahun,
            'income'                => $income, 
            'expense'               => $expense,
            'net_income'            => $income-($expense+$totalbiaya),
            'totalbiaya'            => $expense+$totalbiaya,
            'totalbiayatetap'       => $totalbiayatetap,
            'totalbiayatidaktetap'  => $expense+$totalbiayatidaktetap,
            'data_biaya'            => $bebanbiaya,
            'data_biayatetap'       => $bebanbiayatetap,
        );
        if ($this->input->get("export")==0) {
            $this->load->view('export_laba_rugi', $data);
        }else{
            $this->load->view('print_laba_rugi', $data);
        }
	
	}
}
