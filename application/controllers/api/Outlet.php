<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic outlet interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */

class Outlet extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_outlet');
    }

    function index_post(){
        $api=$this->post('api');
        if($api=="edit") {
            $nama_outlet= $this->post('nama_outlet');
            $deskripsi= $this->post('deskripsi');
            $alamat_outlet= $this->post('alamat_outlet');
            $no_telp= $this->post('no_telp');

            $data = array(  
                "nama_outlet"   => $nama_kategori,
                "alamat_outlet" => $alamat_outlet,
                "deskripsi"     => $deskripsi,
                "no_telp"       => $no_telp,
            );

            $result = $this->M_outlet->update($this->get('kd_outlet'),$data);
            if($result>=0){
                $this->response(['kode' => 1,'pesan' =>'Data Berhasil disimpan!'], REST_Controller::HTTP_OK);
            }else{
                $this->response(['kode' => 2,'pesan' =>'Data gagal diSimpan!'], REST_Controller::HTTP_OK);
            }
        }
    }
    
    function index_get(){
        if ($this->get('api')=="outletdetail") {
            $row = $this->M_outlet->get_by_kd($this->get('kd_outlet'));
            if ($row) {
                $data = array(
                    "kd_outlet"     => $row->kd_outlet,
                    "nama_outlet"   => $row->nama_kategori,
                    "alamat_outlet" => $row->alamat_outlet,
                    "deskripsi"     => $row->deskripsi,
                    "no_telp"       => $row->no_telp,
                );
                $this->response($data, REST_Controller::HTTP_OK);   
            }
        }
    }
}