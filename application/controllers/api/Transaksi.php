<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic transaksi interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */

class Transaksi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_transaksi');
        $this->load->model('M_barang');
    }

    function index_post(){
        $api=$this->post('api');
        if($api=="tambah") {
            $kd_user         = $this->post('kd_user');
            $kd_pelanggan    = $this->post('kd_pelanggan');
            $kd_outlet       = $this->post('kd_outlet');
            $jml_item        = $this->post('jml_item');
            $diskon          = $this->post('diskon');
            $pajak           = $this->post('pajak');
            $harga_total     = $this->post('harga_total');
            $bayar           = $this->post('bayar');
            $kembali         = $this->post('kembali');
            $tgl_transaksi   = date("Y-m-d H:i");
            $status          = $this->post('status');
            $jenis_transaksi = $this->post('jenis_transaksi');

            $data_terakhir  = $this->M_transaksi->get_last($kd_outlet); 
            if(date("Y-m-d")==date("Y-m-d", strtotime($data_terakhir->tgl_transaksi)) and $data_terakhir->jenis_transaksi == $jenis_transaksi){
                $no_invoice     = $data_terakhir->no_invoice+1;
            }else{
                $no_invoice     = 1;    
            }

            $data = array(  
                "kd_transaksi"      => "",
                "no_invoice"        => $no_invoice,
                "kd_user"           => $kd_user,
                "kd_pelanggan"      => $kd_pelanggan,
                "kd_outlet"         => $kd_outlet,
                "jml_item"          => $jml_item,
                "diskon"            => $diskon,
                "pajak"             => $pajak,
                "harga_total"       => $harga_total,
                "bayar"             => $bayar,
                "kembali"           => $kembali,
                "tgl_transaksi"     => $tgl_transaksi,
                "status"            => $status,  
                "jenis_transaksi"   => $jenis_transaksi
            );

            $result = $this->M_transaksi->insert($data);
            if($result>=0){
                $data_terakhir=$this->M_transaksi->get_last($kd_outlet);
                $kd_barang=explode(",", $this->post('kd_barang_keranjang'));
                $qty=explode(",",$this->post('qty_keranjang'));
                $catatan= explode(",",$this->post('catatan'));
                for ($i=0; $i < count($kd_barang); $i++) {
                    $data_barang=$this->M_barang->get_by_kd($kd_barang[$i]); 
                    $data = array(
                        'kd_transaksi'          => $data_terakhir->kd_transaksi,
                        'kd_barang'             => $kd_barang[$i], 
                        'harga_jual_detail'     => $data_barang->harga_jual,
                        'harga_beli_detail'     => $data_barang->harga_beli,
                        'qty'                   => $qty[$i],
                        'catatan'               => $catatan[$i]
                    );
                    if ($data_terakhir->jenis_transaksi==0) {
                        $data_stok = array('stok' => $data_barang->stok-$qty[$i], );
                    }else{
                        $data_stok = array('stok' => $data_barang->stok+$qty[$i], );
                    }

                    $this->M_barang->update($kd_barang[$i],$data_stok);
                    $this->M_transaksi->insert_to_detail($data);
                }
                $this->response(['kode' => 1, 'pesan' =>'Data Berhasil disimpan!', 'kd_transaksi' => $data_terakhir->kd_transaksi, 'kembali' => $data_terakhir->kembali], REST_Controller::HTTP_OK);
            }else{
                $this->response(['kode' => 2,'pesan' =>'Data gagal diSimpan!'], REST_Controller::HTTP_OK);
            }
        }else if($api=="bayar") {
            $data = array( 
                "tgl_transaksi"     => date("Y-m-d H:i"), 
                "bayar"             => $this->post('bayar'),
                "kembali"           => $this->post('kembali'),
                "status"            => 0,
            );
            
            $result = $this->M_transaksi->update($this->post('kd_transaksi'),$data);
            if($result>=0){
                $this->response(['kode' => 1, 'pesan' =>'Status Transaksi Berhasil diupdate!'], REST_Controller::HTTP_OK);
            }else{
                $this->response(['kode' => 2,'pesan' =>'Status Transaksi Gagal diupdate'], REST_Controller::HTTP_OK);
            }
        }
    }
    
    function index_get(){
        if ($this->get('api')=="transaksidetail") {
            $row = $this->M_transaksi->get_by_kd($this->get('kd_transaksi'));
            $diskon_rupiah=($row->diskon/100)*$row->harga_total;
            $pajak_rupiah=($row->pajak/100)*($row->harga_total-$diskon_rupiah);
            $total=($row->harga_total-$diskon_rupiah)+$pajak_rupiah;
            $detailinvoice = $this->M_transaksi->get_detail_transaksi($this->get('kd_transaksi'));
            if ($row) {
                $bulan=date("m", strtotime($row->tgl_transaksi));
                $tanggal=date("d", strtotime($row->tgl_transaksi));
                $tahun=date("Y", strtotime($row->tgl_transaksi));
                $data = array(
                    "kd_transaksi"      => $row->kd_transaksi,
                    "no_invoice"        => $tanggal.$bulan.$tahun.$row->no_invoice,
                    "nama_user"         => $row->nama_depan,
                    "kd_pelanggan"      => $row->kd_pelanggan,
                    "nama_pelanggan"    => $row->nama_pelanggan,
                    "no_telp_pelanggan" => $row->no_telp_pelanggan,
                    "alamat_pelanggan"  => $row->alamat_pelanggan,
                    "tgl_ditambahkan"   => $row->tgl_ditambahkan,
                    "jml_item"          => $row->jml_item,
                    "diskon"            => $row->diskon,
                    "pajak"             => $row->pajak,
                    "diskon_rupiah"     => str_replace(",",".", number_format($diskon_rupiah)), 
                    "pajak_rupiah"      => str_replace(",",".", number_format($pajak_rupiah)),
                    "total"             => str_replace(",",".", number_format($total)), 
                    "harga_total"       => str_replace(",",".", number_format($row->harga_total)),
                    "bayar"             => str_replace(",",".", number_format($row->bayar)),
                    "kembali"           => str_replace(",",".", number_format($row->kembali)),
                    "tgl_transaksi"     => date("d F Y H:i", strtotime($row->tgl_transaksi)),
                    "status"            => $row->status,
                    "jenis_transaksi"   => $row->jenis_transaksi,
                    "detailinvoice"     => $detailinvoice
                );
                $this->response($data, REST_Controller::HTTP_OK);   
            }
        }elseif ($this->get('api')=="detailinvoice") {
            $detailtransaksi = $this->M_transaksi->get_detail_transaksi($this->get('kd_transaksi'));
            foreach ($detailtransaksi as $data_detailtransaksi) {
                if ($data_detailtransaksi->nama_barang==null) {
                    $data_detailtransaksi->nama_barang=" ";
                }
            }
            $data = array(
                'data'     => $detailtransaksi, 
            );
            $this->response($data, REST_Controller::HTTP_OK);
        }elseif ($this->get('api')=="penjualan") {
            $transaksi = $this->M_transaksi->get_all("0","0",$this->get('kd_outlet'),$this->get('periode'));
            $jml_transaksi= $this->M_transaksi->total_rows_perjenis("0","0",$this->get('kd_outlet'),$this->get('periode'));
            $data = array(
                'data'     => $transaksi,
                'jml_data' => $jml_transaksi
            );
            foreach ($transaksi as $data_transaksi) {
                $bulan=date("m", strtotime($data_transaksi->tgl_transaksi));
                $tanggal=date("d", strtotime($data_transaksi->tgl_transaksi));
                $tahun=date("Y", strtotime($data_transaksi->tgl_transaksi));
                $data_transaksi->no_invoice=$tanggal.$bulan.$tahun.$data_transaksi->no_invoice;
                $data_transaksi->tgl_transaksi=date("d F Y H:i", strtotime($data_transaksi->tgl_transaksi));
            }
            $this->response($data, REST_Controller::HTTP_OK);
        }elseif ($this->get('api')=="pembelian") {
            $transaksi = $this->M_transaksi->get_all("1","0",$this->get('kd_outlet'),$this->get('periode'));
            $jml_transaksi= $this->M_transaksi->total_rows_perjenis("1","0",$this->get('kd_outlet'),$this->get('periode'));
            $data = array(
                'data'     => $transaksi,
                'jml_data' => $jml_transaksi
            );
            $couter=0;
            foreach ($transaksi as $data_transaksi) {
                $bulan=date("m", strtotime($data_transaksi->tgl_transaksi));
                $tanggal=date("d", strtotime($data_transaksi->tgl_transaksi));
                $tahun=date("Y", strtotime($data_transaksi->tgl_transaksi));
                $data_transaksi->no_invoice=$tanggal.$bulan.$tahun.$data_transaksi->no_invoice;
                $data_transaksi->tgl_transaksi=date("d F Y H:i", strtotime($data_transaksi->tgl_transaksi));
            }
            $this->response($data, REST_Controller::HTTP_OK);
        }elseif ($this->get('api')=="utang") {
            $transaksi = $this->M_transaksi->get_all("1","1",$this->get('kd_outlet'),$this->get('periode'));
            $jml_transaksi= $this->M_transaksi->total_rows_perjenis("1","1",$this->get('kd_outlet'),$this->get('periode'));
            $data = array(
                'data'     => $transaksi,
                'jml_data' => $jml_transaksi
            );
            foreach ($transaksi as $data_transaksi) {
                $bulan=date("m", strtotime($data_transaksi->tgl_transaksi));
                $tanggal=date("d", strtotime($data_transaksi->tgl_transaksi));
                $tahun=date("Y", strtotime($data_transaksi->tgl_transaksi));
                $data_transaksi->no_invoice=$tanggal.$bulan.$tahun.$data_transaksi->no_invoice;
                $data_transaksi->tgl_transaksi=date("d F Y H:i", strtotime($data_transaksi->tgl_transaksi));
            }
            $this->response($data, REST_Controller::HTTP_OK);
        }elseif ($this->get('api')=="piutang") {
            $transaksi = $this->M_transaksi->get_all("0","1",$this->get('kd_outlet'),$this->get('periode'));
            $jml_transaksi= $this->M_transaksi->total_rows_perjenis("0","1",$this->get('kd_outlet'),$this->get('periode'));
            $data = array(
                'data'     => $transaksi,
                'jml_data' => $jml_transaksi
            );
            foreach ($transaksi as $data_transaksi) {
                $bulan=date("m", strtotime($data_transaksi->tgl_transaksi));
                $tanggal=date("d", strtotime($data_transaksi->tgl_transaksi));
                $tahun=date("Y", strtotime($data_transaksi->tgl_transaksi));
                $data_transaksi->no_invoice=$tanggal.$bulan.$tahun.$data_transaksi->no_invoice;
                $data_transaksi->tgl_transaksi=date("d F Y H:i", strtotime($data_transaksi->tgl_transaksi));
            }
            $this->response($data, REST_Controller::HTTP_OK);
        }elseif ($this->get('api')=="laporan") {
            $jml_data_biaya=0;
            $hari_dari= date("d",strtotime($this->get('dari')));
            $bulan_dari= date("m",strtotime($this->get('dari')));
            $tahun_dari=date("Y",strtotime($this->get('dari')));
            $dari= date("Y-m-d",strtotime($tahun_dari."/".$bulan_dari."/".$hari_dari));
            $hari_sampai= date("d",strtotime($this->get('sampai')))+1;
            $bulan_sampai= date("m",strtotime($this->get('sampai')));
            $tahun_sampai=date("Y",strtotime($this->get('sampai')));
            $sampai= date("Y-m-d",strtotime($tahun_sampai."/".$bulan_sampai."/".$hari_sampai));
            $bulan=$this->get('bulan');
            $tahun=$this->get('tahun');
            if ($this->get("lap")=="laplabarugi") {
                $where="WHERE MONTH(tgl_transaksi)='$bulan' and YEAR(tgl_transaksi)='$tahun'";
            }elseif ($this->get("lap")=="harian") {
                $where="WHERE transaksi.jenis_transaksi='0' and transaksi.tgl_transaksi BETWEEN '$dari' and '$sampai'";
            }elseif ($this->get("lap")=="bulanan") {
                $where="WHERE transaksi.jenis_transaksi='0' and MONTH(transaksi.tgl_transaksi)='$bulan' and YEAR(transaksi.tgl_transaksi)='$tahun'";
            }elseif ($this->get("lap")=="tahunan") {
                $where="WHERE transaksi.jenis_transaksi='0' and YEAR(transaksi.tgl_transaksi)='$tahun'";
            }

            $laporan = $this->M_transaksi->laporan($where,$this->get('kd_outlet'))->result();
            $jml_data= $this->M_transaksi->laporan($where,$this->get('kd_outlet'))->num_rows();

            if ($this->get("lap")=="laplabarugi") {
                $expense=0;
                $income=0;
                $totalbiaya=0;
                $totalbiayatetap=0;
                $totalbiayatidaktetap=0;
                $bebanbiayatetap = $this->M_transaksi->bebanbiaya("WHERE jenis_biaya='0'",$this->get('kd_outlet'))->result();
                $jml_data_biayatetap= $this->M_transaksi->bebanbiaya("WHERE jenis_biaya='0'",$this->get('kd_outlet'))->num_rows();
                $bebanbiaya = $this->M_transaksi->bebanbiaya("WHERE MONTH(tgl_biaya)='$bulan' and YEAR(tgl_biaya)='$tahun' and jenis_biaya='1'",$this->get('kd_outlet'))->result();
                $jml_data_biaya= $this->M_transaksi->bebanbiaya("WHERE MONTH(tgl_biaya)='$bulan' and YEAR(tgl_biaya)='$tahun' and jenis_biaya='1'",$this->get('kd_outlet'))->num_rows();

                foreach ($laporan as $data_laporan) {
                    if ($data_laporan->jenis_transaksi=="0") {
                        $income=$income+$data_laporan->harga_total;
                    }
                    if ($data_laporan->jenis_transaksi=="1") {
                        $expense=$expense+$data_laporan->harga_total;
                    }
                }

                foreach ($bebanbiayatetap as $data_bebanbiayatetap) {
                    if ($data_bebanbiayatetap->jenis_biaya_per==0) {
                        $data_bebanbiayatetap->jumlah_biaya=$data_bebanbiayatetap->jumlah_biaya;
                    }else{
                        $data_bebanbiayatetap->jumlah_biaya=$data_bebanbiayatetap->jumlah_biaya/12;
                    }
                    $totalbiayatetap=$totalbiayatetap+$data_bebanbiayatetap->jumlah_biaya;
                }

                foreach ($bebanbiaya as $data_bebanbiaya) {
                    $totalbiayatidaktetap=$totalbiayatidaktetap+$data_bebanbiaya->jumlah_biaya;
                }

                $totalbiaya=$totalbiayatetap+$totalbiayatidaktetap;

                $data = array(
                    'income'                => $income, 
                    'expense'               => $expense,
                    'net_income'            => $income-($expense+$totalbiaya),
                    'totalbiaya'            => $expense+$totalbiaya,
                    'totalbiayatetap'       => $totalbiayatetap,
                    'totalbiayatidaktetap'  => $expense+$totalbiayatidaktetap,
                    'data_biaya'            => $bebanbiaya,
                    'data_biayatetap'       => $bebanbiayatetap,
                );
            }else{
                $pendapatan=0;
                foreach ($laporan as $data_laporan) {
                    $pendapatan=$pendapatan+$data_laporan->harga_total;
                }
                $data = array(
                    'pendapatan' => $pendapatan,
                    'jml_transaksi' => $jml_data,
                    'total_harga_semua' => $pendapatan,
                    'barangTerjual' => $this->M_transaksi->get_barang_terjual($where,$this->get('kd_outlet')),
                );
            }

            $data = array(
                'data'     => $data,
                'jml_data' => $jml_data+$jml_data_biaya
            );
            $this->response($data, REST_Controller::HTTP_OK);
        }
    }
}