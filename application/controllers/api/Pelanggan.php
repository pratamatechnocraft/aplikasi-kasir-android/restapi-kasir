<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic pelanggan interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */

class Pelanggan extends REST_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_pelanggan');
    }

    function index_post(){
        $api=$this->post('api');
        if($api=="tambah") {
            $kd_outlet= $this->post('kd_outlet');
            $nama_pelanggan= $this->post('nama_pelanggan');
            $no_telp_pelanggan=$this->post('no_telp_pelanggan');
            $alamat_pelanggan=$this->post('alamat_pelanggan');

            $data = array(  
                "kd_pelanggan"          => "",
                "kd_outlet"             => $kd_outlet,
                "nama_pelanggan"        => $nama_pelanggan,
                "no_telp_pelanggan"     => $no_telp_pelanggan,
                "alamat_pelanggan"      => $alamat_pelanggan,
                'tgl_ditambahkan'       => date("Y-m-d")
            );

            $result = $this->M_pelanggan->insert($data);
            if($result>=0){
                $data_terakhir=$this->M_pelanggan->get_last($kd_outlet);
                $this->response([
                    'kode'              => 1,
                    'pesan'             =>'Data Berhasil disimpan!', 
                    'kd_pelanggan'      => $data_terakhir->kd_pelanggan,
                    'nama_pelanggan'    => $data_terakhir->nama_pelanggan,
                    'no_telp_pelanggan' => $data_terakhir->no_telp_pelanggan,
                    'alamat_pelanggan'  => $data_terakhir->alamat_pelanggan,
                    'tgl_ditambahkan'   => $data_terakhir->tgl_ditambahkan
                ], REST_Controller::HTTP_OK);
            }else{
                $this->response(['kode' => 2,'pesan' =>'Data gagal diSimpan!'], REST_Controller::HTTP_OK);
            }
        }else if($api=="edit") {
            $nama_pelanggan= $this->post('nama_pelanggan');
            $no_telp_pelanggan=$this->post('no_telp_pelanggan');
            $alamat_pelanggan=$this->post('alamat_pelanggan');

            $data = array(  
                'nama_pelanggan'    => $nama_pelanggan, 
                'no_telp_pelanggan' => $no_telp_pelanggan,
                'alamat_pelanggan'  => $alamat_pelanggan
            );
            $result = $this->M_pelanggan->update($this->post('kd_pelanggan'), $data);
            if($result>=0){
                $this->response(['kode' => 1, 'pesan' =>'Data Berhasil disimpan!'], REST_Controller::HTTP_OK);
            }else{
                $this->response(['kode' => 2,'pesan' =>'Data gagal diSimpan!'], REST_Controller::HTTP_OK);
            }
        }
    }
    
    function index_get(){
        if ($this->get('api')=="pelanggandetail") {
            $row = $this->M_pelanggan->get_by_kd($this->get('kd_pelanggan'));
            if ($row) {
                $data = array(
                    "kd_pelanggan"          => $row->kd_pelanggan,
                    "nama_pelanggan"        => $row->nama_pelanggan,
                    "no_telp_pelanggan"     => $row->no_telp_pelanggan,
                    "alamat_pelanggan"      => $row->alamat_pelanggan,
                    'tgl_ditambahkan'       => date("d F Y", strtotime($row->tgl_ditambahkan)),
                );
                $this->response($data, REST_Controller::HTTP_OK);   
            }
        }elseif ($this->get('api')=="pelangganall") {
            $pelanggan = $this->M_pelanggan->get_where(array('kd_outlet' => $this->get('kd_outlet')));
            $jml_pelanggan= $this->M_pelanggan->total_rows_where(array('kd_outlet' => $this->get('kd_outlet')),0);
           /* foreach ($pelanggan as $data_pelanggan) {
                $data_pelanggan->tgl_ditambahkan = date("d F Y", strtotime($data_pelanggan->tgl_ditambahkan));
            }*/
            
            $data = array(
                'data'     => $pelanggan,
                'jml_data' => $jml_pelanggan
            );
            $this->response($data, REST_Controller::HTTP_OK);
        }elseif ($this->get('api')=="delete") {
            $result = $this->M_pelanggan->delete($this->get('kd_pelanggan'));
            if($result>=0){
                $this->response(['kode' => 1, 'pesan' =>'Data Berhasil dihapus!'], REST_Controller::HTTP_OK);
            }else{
                $this->response(['kode' => 2,'pesan' =>'Data gagal dihapus!'], REST_Controller::HTTP_OK);
            }
        }
    }
}