<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Print_invoice extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
    {
        parent::__construct();
        $this->load->model('M_transaksi');
        $this->load->model('M_barang');
        $this->load->model('M_outlet');
    }

	public function index()
	{
		
		$row = $this->M_transaksi->get_by_kd($this->input->get('no_invoice'));
        $detailinvoice = $this->M_transaksi->get_detail_transaksi($this->input->get('no_invoice'));
        $diskon_rupiah=($row->diskon/100)*$row->harga_total;
        $pajak_rupiah=($row->pajak/100)*($row->harga_total-$diskon_rupiah);
        $total=($row->harga_total-$diskon_rupiah)+$pajak_rupiah;
        $outlet=$this->M_outlet->get_by_kd($row->kd_outlet);

        $data = array(
            'nama_outlet'       => $outlet->nama_outlet,
            'alamat_outlet'     => $outlet->alamat_outlet,
            'deskripsi_outlet'  => $outlet->deskripsi,
            'no_telp_outlet'    => $outlet->no_telp,
            "kd_transaksi"      => $row->kd_transaksi,
            "nama_user"         => $row->nama_depan,
            "nama_pelanggan"    => $row->nama_pelanggan,
            "jml_item"          => $row->jml_item,
            "diskon"          	=> $row->diskon,
            "pajak"          	=> $row->pajak,
            "diskon_rupiah"     => str_replace(",",".", number_format($diskon_rupiah)),	
            "pajak_rupiah"     	=> str_replace(",",".", number_format($pajak_rupiah)),
            "total"				=> str_replace(",",".", number_format($total)),	
            "harga_total"       => str_replace(",",".", number_format($row->harga_total)),
            "bayar"             => str_replace(",",".", number_format($row->bayar)),
			"kembali"           => str_replace(",",".", number_format($row->kembali)),
            "tgl_transaksi"     => date("d F Y H:i", strtotime($row->tgl_transaksi)),
            "status"            => $row->status,
            "jenis_transaksi"   => $row->jenis_transaksi,
            "detailinvoice"     => $detailinvoice,
        );
		$this->load->view('print_invoice',$data);
	}
}
