<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_transaksi extends CI_Model
{

    public $table = 'transaksi';
    public $kd = 'kd_transaksi';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all($jenis_transaksi,$status,$kd_outlet,$periode=NULL) {
        if ($periode!=NULL AND $periode!="Semua") {
            $tanggal_awal=date("Y-m-d");
            if ($periode=="Hari Ini") {
                $tanggal_awal=date('Y-m-d', strtotime('+1 days', strtotime($tanggal_awal)));
                $tanggal_akhir=date('Y-m-d');
            }elseif ($periode=="1 Minggu Terakhir") {
                $tanggal_akhir=date('Y-m-d', strtotime('-7 days', strtotime($tanggal_awal)));
            }elseif ($periode=="1 Bulan Terakhir") {
                $tanggal_akhir=date('Y-m-d', strtotime('-1 month', strtotime($tanggal_awal)));
            }elseif ($periode=="1 Tahun Terakhir") {
                $tanggal_akhir=date('Y-m-d', strtotime('-1 year', strtotime($tanggal_awal)));
            }
            $this->db->where('tgl_transaksi >=', $tanggal_akhir);
            $this->db->where('tgl_transaksi <=', $tanggal_awal);
        }
    	$this->db->where('jenis_transaksi', $jenis_transaksi);
        $this->db->where('kd_outlet', $kd_outlet);
    	$this->db->where('status', $status);
        $this->db->order_by("tgl_transaksi", $this->order);
        return $this->db->get($this->table)->result();
    }
    function get_where($where){
        $this->db->where($where);
        $this->db->order_by($this->kd, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by kd
    function get_by_kd($kd){
        $this->db->where($this->kd, $kd);
        $this->db->join("user", "user.kd_user=transaksi.kd_user","left");
        $this->db->join("pelanggan", "pelanggan.kd_pelanggan=transaksi.kd_pelanggan","left");
        return $this->db->get($this->table)->row();
    }

    function get_last($kd_outlet){
        $this->db->where('kd_outlet', $kd_outlet);
        $this->db->order_by($this->kd, $this->order);
        return $this->db->get($this->table)->row();
    }

    function get_detail_transaksi($kd) {
        $this->db->where('kd_transaksi', $kd);
        $this->db->order_by($this->kd, $this->order);
        $this->db->join("barang", "barang.kd_barang=detail_transaksi.kd_barang","left");
        return $this->db->get("detail_transaksi")->result();
    }

    function get_barang_terjual($where,$kd_outlet){
        /*$this->db->join("barang", "barang.kd_barang=detail_transaksi.kd_barang","left");
        $this->db->join("transaksi", "detail_transaksi.kd_transaksi=transaksi.kd_transaksi","left");
        $this->db->group_by("kd_barang");*/
        return $this->db->query("SELECT sum(qty) as qty, nama_barang, harga_jual_detail FROM detail_transaksi LEFT JOIN transaksi ON detail_transaksi.kd_transaksi=transaksi.kd_transaksi LEFT JOIN barang ON barang.kd_barang=detail_transaksi.kd_barang ".$where." AND transaksi.kd_outlet=".$kd_outlet." GROUP BY detail_transaksi.kd_barang ORDER BY qty DESC")->result();
    }

    // get data by transaksi
    /*function get_by_kat($kat)
    {
        $this->db->where($this->kat, $kat);
        return $this->db->get($this->table)->result();
    }*/

     // get total rows
    function total_rows($kd_outlet,$limit,$q = NULL) {
        $this->db->where('kd_outlet', $kd_outlet);
        $this->db->like('kd_transaksi', $q);
        $this->db->or_like('tgl_transaksi', $q);
        $this->db->or_like('harga_total', $q);
        $this->db->or_like('jenis_transaksi', $q);
        $this->db->or_like('status_transaksi', $q);
        return $this->db->get($this->table,$limit)->num_rows();
    }

    function total_rows_perjenis($jenis_transaksi,$status,$kd_outlet,$periode=NULL) {
        if ($periode!=NULL AND $periode!="Semua") {
            $tanggal_awal=date("Y-m-d");
            if ($periode=="Hari Ini") {
                $tanggal_awal=date('Y-m-d', strtotime('+1 days', strtotime($tanggal_awal)));
                $tanggal_akhir=date('Y-m-d');
            }elseif ($periode=="1 Minggu Terakhir") {
                $tanggal_akhir=date('Y-m-d', strtotime('-7 days', strtotime($tanggal_awal)));
            }elseif ($periode=="1 Bulan Terakhir") {
                $tanggal_akhir=date('Y-m-d', strtotime('-1 month', strtotime($tanggal_awal)));
            }elseif ($periode=="1 Tahun Terakhir") {
                $tanggal_akhir=date('Y-m-d', strtotime('-1 year', strtotime($tanggal_awal)));
            }
            $this->db->where('tgl_transaksi >=', $tanggal_akhir);
            $this->db->where('tgl_transaksi <=', $tanggal_awal);
        }
        $this->db->where('kd_outlet', $kd_outlet);
    	$this->db->where('jenis_transaksi', $jenis_transaksi);
    	$this->db->where('status', $status);
        return $this->db->get($this->table)->num_rows();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL, $nama_kolom='kd_transaksi', $order='DESC') {
       $this->db->order_by($nama_kolom, $order);
       $this->db->like('kd_transaksi', $q);
       $this->db->or_like('tgl_transaksi', $q);
       $this->db->or_like('harga_total', $q);
       $this->db->or_like('jenis_transaksi', $q);
       $this->db->or_like('status_transaksi', $q);
       $this->db->limit($limit, $start);
       return $this->db->get($this->table)->result();
    }

    function laporan($where,$kd_outlet){
        return $this->db->query("SELECT * FROM transaksi ".$where." AND transaksi.kd_outlet=".$kd_outlet);
    }

    function bebanbiaya($where,$kd_outlet){
        return $this->db->query("SELECT * FROM biaya ".$where." AND biaya.kd_outlet=".$kd_outlet);
    }


    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // insert data
    function insert_to_detail($data)
    {  
        $this->db->insert("detail_transaksi", $data);
    }

    // update data
    function update($kd, $data)
    {
        $this->db->where($this->kd, $kd);
        $this->db->update($this->table, $data);
    }
}