<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class M_auth extends CI_Model{      

    public $client_service='tokason-client';

    function check_auth_client(){
    	$input_client_service = $this->input->get_request_header('Client_Service', TRUE);
    	if ($input_client_service == $this->client_service) {
    		return TRUE;
    	}else{
    		return FALSE;
    	}
    }

    function auth($kd_user){
        $token_api_key=$this->input->get_request_header('Authorization', TRUE);
        if ($kd_user=="") {
            return array('success' => 401, 'pesan' => "Unauthorized Header", );
        }elseif ($token_api_key=="") {
            return array('success' => 401, 'pesan' => "Unauthorized Header", );
        }else{
            $cek_id=$this->db->where('kd_user', $kd_user)->get('user')->num_rows();
            $data_user_where_id=$this->db->where('kd_user', $kd_user)->get('user')->row();
            if ($cek_id > 0) {
                $where = array(
                    'kd_user'       => $data_user_where_id->kd_user,
                    'token_api_key' => $token_api_key, 
                );
                $cek_token_api_key=$this->db->get_where('user', $where)->num_rows();
                if ($cek_token_api_key > 0) {
                    return array('success' => 200, 'pesan' => "Aauthorized", );
                }else{
                    $data_token_api_key = array('token_api_key' => $token_api_key,);
                    $this->db->where('kd_user', $kd_user)->update('user', $data_token_api_key);
                    return array('success' => 200, 'pesan' => "Aauthorized", );
                }
            }else{
                return array('success' => 401, 'pesan' => "Unauthorized Header", );
            }
            
        }
    }
}