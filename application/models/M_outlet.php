<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_outlet extends CI_Model
{

    public $table = 'outlet';
    public $kd = 'kd_outlet';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->kd, $this->order);
        return $this->db->get($this->table)->result();
    }
    function get_where($where=NULL, $limit=0)
    {
        if($where!=NULL){
            $this->db->where($where);
        }
        $this->db->order_by($this->kd, $this->order);
        return $this->db->get($this->table, $limit);
    }

    // get data by kd
    function get_by_kd($kd)
    {
        $this->db->where($this->kd, $kd);
        return $this->db->get($this->table)->row();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL, $nama_kolom='kd_outlet', $order='DESC') {
       $this->db->order_by($nama_kolom, $order);
       $this->db->like('kd_outlet', $q);
       $this->db->or_like('nama_outlet', $q);
       $this->db->or_like('alamat_outlet', $q);
       $this->db->limit($limit, $start);
       return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($kd, $data)
    {
        $this->db->where($this->kd, $kd);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($kd)
    {
        $this->db->where($this->kd, $kd);
        $this->db->delete($this->table);
    }

}