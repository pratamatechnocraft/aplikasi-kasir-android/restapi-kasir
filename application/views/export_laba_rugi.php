<?php 
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=LaporanLabaRugi_".$periode.".xls");
?>

<H3><?php echo $nama_outlet; ?></H3>
<h4>Laporan Laba Rugi</h4>
<h5>Periode <?php echo $periode; ?></h5>

<table style="border-collapse: collapse;" cellpadding="5" width="100%" border="1">
	<tr>
		<td width="33%" style="font-weight:bold;"><u>Pendapatan</u></td>
		<td width="33%"></td>
		<td width="33%"></td>
	</tr>
	<tr>
		<td>Penjualan</td>
		<td align="right">
			
		</td>
		<td align="right">
			<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp. <?php echo str_replace(",", '.', number_format($income)); ?></u></br>
		</td>
	</tr>
	<tr>
		<td style="font-weight:bold;">Laba Kotor</td>
		<td align="right">
			
		</td>
		<td align="right" style="font-weight:bold;">
			Rp. <?php echo str_replace(",", '.', number_format($income)); ?>
		</td>
	</tr>
	<tr>
		<td style="font-weight:bold;"><u>Biaya Tidak Tetap</u></td>
		<td></td>
		<td></td>
	</tr>
	<?php foreach ($data_biaya as $biaya): ?>
	<tr>
		<td><?php echo $biaya->nama_biaya; ?></td>
		<td align="right">
			Rp. <?php echo str_replace(",", '.', number_format($biaya->jumlah_biaya)); ?>
		</td>
		<td align="right">
			
		</td>
	</tr>	
	<?php endforeach ?>
	<tr>
		<td>Biaya Pengeluaran Lainya</td>
		<td align="right">
			<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp. <?php echo $expense; ?></u>
		</td>
		<td align="right">
			
		</td>
	</tr>	
	<tr>
		<td style="font-weight:bold;">Total Biaya Tidak Tetap :</td>
		<td align="right">
		</td>
		<td align="right" style="font-weight:bold;">
			<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp. <?php echo str_replace(",", '.', number_format($totalbiayatidaktetap)); ?></u>
		</td>
	</tr>
	<tr>
		<td style="font-weight:bold;"><u>Biaya Tidak Tetap</u></td>
		<td></td>
		<td></td>
	</tr>
	<?php foreach ($data_biayatetap as $biayatetap): ?>
	<tr>
		<td><?php echo $biayatetap->nama_biaya; ?></td>
		<td align="right">
			Rp. <?php echo str_replace(",", '.', number_format($biayatetap->jumlah_biaya)); ?>
		</td>
		<td align="right">
			
		</td>
	</tr>	
	<?php endforeach ?>
	<tr>
		<td style="font-weight:bold;">Total Biaya Tetap :</td>
		<td align="right">
		</td>
		<td align="right" style="font-weight:bold;">
			<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp. <?php echo str_replace(",", '.', number_format($totalbiayatetap)); ?></u>
		</td>
	</tr>
	<tr>
		<td style="font-weight:bold;">Total Biaya :</td>
		<td align="right">
		</td>
		<td align="right" style="font-weight:bold;">
			<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp. <?php echo str_replace(",", '.', number_format($totalbiaya)); ?></u>
		</td>
	</tr>
	<tr>
		<td style="font-weight:bold;">Laba Bersih</td>
		<td align="right">
		</td>
		<td align="right" style="font-weight:bold;">
			Rp. <?php echo str_replace(",", '.', number_format($net_income)); ?>
		</td>
	</tr>
</table>
