<!DOCTYPE html>
<html>
<head>
	<style type="text/css" media="print">
        @page { 
            width: 100%;
            margin:0;
        }
    </style> 
	<style type="text/css">
		body{
			font: 9pt "Tahoma";
		}
		.container{
			overflow: hidden;
		}
		.head{
			text-align: center;
			margin-bottom: 20px;
		}
		.nama{
			font-size: 14pt;
		}
		.foot{
			font-size: 9pt;
			text-align: center;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="head">
			<p>
				<span class="nama"><?php echo $nama_outlet; ?></span><br>
				<?php echo $alamat_outlet; ?></br>
				<?php echo $deskripsi_outlet; ?></br>
				<?php echo $no_telp_outlet; ?>
			</p>
		</div>
		<table>
			<tr>
				<td>Tanggal</td>
				<td>:</td>
				<td><?php echo $tgl_transaksi; ?></td>
			</tr>
			<tr>
				<td>No Invoice</td>
				<td>:</td>
				<td>
					<?php 
						if ($jenis_transaksi==0){
							echo "#PL".$kd_transaksi;
						}else{
							echo "#PB".$kd_transaksi;
						} 
					?>	
				</td>
			</tr>
			<?php 
				if ($jenis_transaksi==0) {
					?>
						<tr>
							<td>Pelanggan</td>
							<td>:</td>
							<td><?php echo $nama_pelanggan; ?></td>
						</tr>
					<?php
				}
			?>
			<tr>
				<td>Kasir</td>
				<td>:</td>
				<td><?php echo $nama_user; ?></td>
			</tr>
			<tr>
				<td>Jml Item</td>
				<td>:</td>
				<td><?php echo $jml_item; ?></td>
			</tr>
			<tr>
				<td>Status</td>
				<td>:</td>
				<td>
					<?php 
						if ($status==0){
							echo "Lunas";
						}else{
							echo "Belum Lunas";
						} 
					?>
				</td>
			</tr>
		</table>
		<table style="border-collapse: collapse;" width="100%">
			<tr>
				<td colspan="2" align="center">===========================</td>
			</tr>
			<tr>
				<td style="text-align: left;">Barang</td>
				<td style="text-align: right;">Total Harga</td>
			</tr>
			<tr>
				<td colspan="2" align="center">===========================</td>
			</tr>
			<?php foreach ($detailinvoice as $data_detailinvoice) { ?>
				<?php 
					if ($jenis_transaksi==0) {
						$harga=$data_detailinvoice->harga_jual_detail;
					}else{
						$harga=$data_detailinvoice->harga_jual_detail;
					}
				?>
				<tr>
					<td colspan="2">
						<?php echo $data_detailinvoice->nama_barang; ?>
						<?php 
							if ($data_detailinvoice->catatan!="") {
								echo "(".$data_detailinvoice->catatan.")";
							}
						?>
					</td>
				</tr>
				<tr>
					<td>
						<span><?php echo str_replace(",", ".", number_format($data_detailinvoice->qty)); ?></span> x @Rp. <span><?php echo str_replace(",", ".", number_format($harga)); ?></span>
					</td>
					<td align="top" style="text-align: right;">Rp. <?php echo str_replace(",", ".", number_format($data_detailinvoice->qty*$harga)); ?></td>
				</tr>
			<?php } ?>
			<tr>
				<td colspan="2" align="center">===========================</td>
			</tr>
			<tr>
				<td style="text-align: right;">Sub Total : </td>
				<td style="text-align: right;">Rp. <?php echo $harga_total; ?></td>
			</tr>
			<tr>
				<td style="text-align: right;">Diskon (<?php echo $diskon."%"; ?>) : </td>
				<td style="text-align: right;">Rp. <?php echo $diskon_rupiah; ?></td>
			</tr>
			<tr>
				<td style="text-align: right;">Pajak (<?php echo $pajak."%"; ?>) : </td>
				<td style="text-align: right;">Rp. <?php echo $pajak_rupiah; ?></td>
			</tr>
			<tr>
				<td style="text-align: right;">Total : </td>
				<td style="text-align: right;">Rp. <?php echo $total; ?></td>
			</tr>
			<tr>
				<td style="text-align: right;">Tunai : </td>
				<td style="text-align: right;">Rp. <?php echo $bayar; ?></td>
			</tr>
			<tr>
				<td style="text-align: right;">Kembali : </td>
				<td style="text-align: right;">Rp. <?php echo $kembali; ?></td>
			</tr>
			<tr>
				<td colspan="2" align="center">===========================</td>
			</tr>
		</table>
		<div class="foot" align="center">
			<p>
				TERIMA KASIH
			</p>
		</div>
	</div>
</body>
</html>