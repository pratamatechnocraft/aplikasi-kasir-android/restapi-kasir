-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 17, 2019 at 09:06 PM
-- Server version: 10.1.41-MariaDB-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pratamat_kasirsrc`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kd_barang` int(15) NOT NULL,
  `kd_kategori` int(10) NOT NULL,
  `kd_outlet` int(8) UNSIGNED ZEROFILL NOT NULL,
  `barcode` varchar(40) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `harga_jual` decimal(10,0) NOT NULL,
  `harga_beli` decimal(10,0) NOT NULL,
  `stok` decimal(10,0) NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar_barang` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kd_barang`, `kd_kategori`, `kd_outlet`, `barcode`, `nama_barang`, `harga_jual`, `harga_beli`, `stok`, `deskripsi`, `gambar_barang`) VALUES
(1, 5, 00000001, '8993988130437', 'Joyko Highlighter Double Color Warna Biru', '2000', '1700', '30', 'Stabilo 2 warna', 'assets/images/upload/barang/barang_Joyko Highlighter Double Color Warna Biru.jpeg'),
(2, 1, 00000001, '8992952925659', 'Cho Cho Radja Wafer Roll Rasa Cokelat', '35000', '27950', '9', 'Wafer Rasa Cokelat', 'assets/images/upload/barang/barang_Cho Cho Radja Wafer Roll Rasa Cokelat.jpeg'),
(3, 1, 00000001, '8888166995185', 'Nissin Egg Roll Sesame Rasa Wijen', '42000', '40000', '18', '-', ''),
(4, 2, 00000001, '8993365121539', 'Madu TJ', '2000', '1000', '25', '-', '');

-- --------------------------------------------------------

--
-- Table structure for table `biaya`
--

CREATE TABLE `biaya` (
  `kd_biaya` int(11) NOT NULL,
  `kd_outlet` int(8) UNSIGNED ZEROFILL NOT NULL,
  `nama_biaya` varchar(100) NOT NULL,
  `jumlah_biaya` decimal(10,0) NOT NULL,
  `tgl_biaya` varchar(50) NOT NULL,
  `jenis_biaya` int(2) NOT NULL,
  `jenis_biaya_per` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biaya`
--

INSERT INTO `biaya` (`kd_biaya`, `kd_outlet`, `nama_biaya`, `jumlah_biaya`, `tgl_biaya`, `jenis_biaya`, `jenis_biaya_per`) VALUES
(1, 00000001, 'Sewa Bangunan Baru', '5000000', '1970-01-01 07:00', 0, 1),
(2, 00000001, 'Listrik', '200000', '2019-08-09 15:48', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `kd_transaksi` int(10) UNSIGNED ZEROFILL NOT NULL,
  `kd_barang` int(15) NOT NULL,
  `harga_jual_detail` decimal(10,0) NOT NULL,
  `harga_beli_detail` decimal(10,0) NOT NULL,
  `qty` int(10) NOT NULL,
  `catatan` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_transaksi`
--

INSERT INTO `detail_transaksi` (`kd_transaksi`, `kd_barang`, `harga_jual_detail`, `harga_beli_detail`, `qty`, `catatan`) VALUES
(0000000001, 1, '2000', '1700', 20, ''),
(0000000003, 1, '2000', '1700', 5, ''),
(0000000004, 1, '2000', '1700', 1, ''),
(0000000005, 1, '2000', '1700', 2, ''),
(0000000005, 2, '35000', '27950', 1, ''),
(0000000006, 3, '42000', '40000', 1, ''),
(0000000007, 3, '42000', '40000', 1, ''),
(0000000008, 1, '2000', '1700', 1, ''),
(0000000009, 1, '2000', '1700', 1, ''),
(0000000010, 1, '2000', '1700', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `kd_kategori` int(10) NOT NULL,
  `kd_outlet` int(8) UNSIGNED ZEROFILL NOT NULL,
  `nama_kategori` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`kd_kategori`, `kd_outlet`, `nama_kategori`) VALUES
(1, 00000001, 'Makanan'),
(2, 00000001, 'Minumam'),
(3, 00000002, 'Makanan'),
(6, 00000002, 'minuman'),
(5, 00000001, 'ATK');

-- --------------------------------------------------------

--
-- Table structure for table `outlet`
--

CREATE TABLE `outlet` (
  `kd_outlet` int(8) UNSIGNED ZEROFILL NOT NULL,
  `nama_outlet` varchar(50) NOT NULL,
  `alamat_outlet` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outlet`
--

INSERT INTO `outlet` (`kd_outlet`, `nama_outlet`, `alamat_outlet`) VALUES
(00000001, 'SRC KETAPANG 1', 'Ketapang\r\n'),
(00000002, 'SRC JEMBER 1', 'Jl. Riau Jember');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `kd_pelanggan` int(10) UNSIGNED ZEROFILL NOT NULL,
  `kd_outlet` int(8) UNSIGNED ZEROFILL NOT NULL,
  `nama_pelanggan` varchar(40) NOT NULL,
  `no_telp_pelanggan` varchar(50) NOT NULL,
  `alamat_pelanggan` text NOT NULL,
  `tgl_ditambahkan` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`kd_pelanggan`, `kd_outlet`, `nama_pelanggan`, `no_telp_pelanggan`, `alamat_pelanggan`, `tgl_ditambahkan`) VALUES
(0000000001, 00000001, 'Vyan', '0815567808103', 'Banyuwangi', '2018-12-11 '),
(0000000002, 00000002, 'Ary', '081556780821', 'Jember', '2019-04-27'),
(0000000003, 00000002, 'Yusuf', '088227968', 'Jember', '2019-05-01'),
(0000000004, 00000001, 'Lina', '0789598', 'Lateng', '2019-05-01'),
(0000000010, 00000002, 'Linda', '05757676', 'Coba', '2019-05-03'),
(0000000011, 00000002, 'bahrul', '989989', 'disana', '2019-05-12');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `kd_transaksi` int(10) UNSIGNED ZEROFILL NOT NULL,
  `no_invoice` int(4) UNSIGNED ZEROFILL NOT NULL,
  `kd_user` int(10) NOT NULL,
  `kd_outlet` int(8) UNSIGNED ZEROFILL NOT NULL,
  `kd_pelanggan` int(10) UNSIGNED ZEROFILL NOT NULL,
  `tgl_transaksi` varchar(100) NOT NULL,
  `jml_item` int(10) NOT NULL,
  `diskon` decimal(10,0) NOT NULL,
  `pajak` int(10) NOT NULL,
  `harga_total` decimal(10,0) NOT NULL,
  `bayar` decimal(10,0) NOT NULL,
  `kembali` decimal(10,0) NOT NULL,
  `jenis_transaksi` int(2) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`kd_transaksi`, `no_invoice`, `kd_user`, `kd_outlet`, `kd_pelanggan`, `tgl_transaksi`, `jml_item`, `diskon`, `pajak`, `harga_total`, `bayar`, `kembali`, `jenis_transaksi`, `status`) VALUES
(0000000001, 0001, 1, 00000001, 0000000000, '2019-08-09 15:40', 20, '0', 0, '34000', '40000', '6000', 1, 0),
(0000000003, 0002, 1, 00000001, 0000000000, '2019-08-09 15:41', 5, '0', 0, '8500', '8500', '0', 1, 0),
(0000000004, 0003, 1, 00000001, 0000000000, '2019-08-09 15:42', 1, '0', 0, '1700', '0', '0', 1, 1),
(0000000005, 0001, 1, 00000001, 0000000001, '2019-08-09 15:46', 3, '0', 0, '39000', '50000', '11000', 0, 0),
(0000000006, 0002, 1, 00000001, 0000000004, '2019-08-09 22:29', 1, '0', 0, '42000', '50000', '8000', 0, 0),
(0000000007, 0003, 1, 00000001, 0000000001, '2019-08-09 22:30', 1, '0', 0, '42000', '100000', '58000', 0, 0),
(0000000008, 0004, 1, 00000001, 0000000001, '2019-08-09 22:56', 1, '0', 0, '2000', '2000', '0', 0, 0),
(0000000009, 0001, 1, 00000001, 0000000000, '2019-08-09 22:57', 1, '0', 0, '1700', '2000', '300', 1, 0),
(0000000010, 0002, 1, 00000001, 0000000000, '2019-08-09 22:57', 1, '0', 0, '1700', '0', '0', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `kd_user` int(10) NOT NULL,
  `kd_outlet` int(8) UNSIGNED ZEROFILL NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(200) NOT NULL,
  `nama_depan` varchar(40) NOT NULL,
  `nama_belakang` varchar(40) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `foto` text NOT NULL,
  `level_user` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`kd_user`, `kd_outlet`, `username`, `password`, `nama_depan`, `nama_belakang`, `no_telp`, `alamat`, `foto`, `level_user`) VALUES
(1, 00000001, 'vyan', '30519696d99c9ba98b251f293eb3d96f1d2bf1c3eee4c487cdb707ab2285de56ce859a30a4d188f920a53e74ae4f9421eb4d1dd29f19e87d015fcb450fb67612vP1ZGdMFQsY37tA28TdXFGO4RSY=', 'Owner', 'Toko', '081556780810', 'Jember', 'assets/images/upload/user/user.png', 0),
(2, 00000001, 'vyankasir', '30519696d99c9ba98b251f293eb3d96f1d2bf1c3eee4c487cdb707ab2285de56ce859a30a4d188f920a53e74ae4f9421eb4d1dd29f19e87d015fcb450fb67612vP1ZGdMFQsY37tA28TdXFGO4RSY=', 'Kasir', 'Kantin', '081556780810', 'Jl. Riau No 22', 'assets/images/upload/user/user.png', 1),
(10, 00000002, 'ary', '30519696d99c9ba98b251f293eb3d96f1d2bf1c3eee4c487cdb707ab2285de56ce859a30a4d188f920a53e74ae4f9421eb4d1dd29f19e87d015fcb450fb67612vP1ZGdMFQsY37tA28TdXFGO4RSY=', 'Ary ', 'Pratama', '081557890910', 'Jember ', 'assets/images/upload/user/10_190809155048.jpeg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kd_barang`);

--
-- Indexes for table `biaya`
--
ALTER TABLE `biaya`
  ADD PRIMARY KEY (`kd_biaya`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`kd_kategori`);

--
-- Indexes for table `outlet`
--
ALTER TABLE `outlet`
  ADD PRIMARY KEY (`kd_outlet`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`kd_pelanggan`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`kd_transaksi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`kd_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `kd_barang` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `biaya`
--
ALTER TABLE `biaya`
  MODIFY `kd_biaya` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `kd_kategori` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `outlet`
--
ALTER TABLE `outlet`
  MODIFY `kd_outlet` int(8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `kd_pelanggan` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `kd_transaksi` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `kd_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
